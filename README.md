# Introduction
What follows are several problems regarding programming.  This is an assessment to determine how you approach and solve problems, not a test or examination.  Your answers will not be graded, and no score will be generated or considered as part of your application (there is not even an answer key, just as there is not in real life). Reviewers may ask questions about why or how you approached certain problems as part of your second interview.

As your answer, please edit this repository by adding a `ANSWER.md` file at the root to contain any written prose the problems ask for, and edit the code examples in line.  For the queries you need to write, put them, one per file, in a `src/sql` directory.  Once you are done, create a `tar.gz` archive of this directory (which will constitute your "answer" to the assessment), and upload it to the Google Drive dropbox (your HR representative should have provided a link).

Use whatever resources you would have available to you during a normal day at work.  If you are unsure about how to fix something, at least note that problem in your answer sheet or code, and indicate why you were unsure of the right fix.

## Code Review

### Part 1: Java
There is a java source code file in `src/java`.  This is a Java program that counts the number of times a word occurs in a file.  It is a command line application that takes two arguments: the name of an input file, and an integer threshold.  The file should contain one word per line, with no whitespace at the start or end of the word (the utility does not need to validate the format of the file).  Words are tallied case-insensitively.  Once the script is done counting, it should output an alphabetically sorted report of each word that occurred more times than the threshold, along with the number of recurrences.

An example of running the command is below:

```bash
java -cp "./*" Test resources/words.txt 50
```

We have provided an input file of the correct form: `resources/words.txt`.

Your job is review this code.  Make any bug fixes or necessary improvements (performance, maintainability, etc), and provide us with your fixed version of the code.  In addition, please explain the problems you found, the changes you made, and why.


### Part 2: JavaScript
There is also a JavaScript file called `sample.js` in `src/js`.  This script is meant to be loaded onto an imaginary page in LabKey server.  You can assume there is a button on that page with the id mentioned in the script, as well as that the **&lt;div&gt;** mentioned exists, and that the LabKey and ExtJS (version 4) libraries have been appropriately loaded before this script and are available in the global namespace.  You also have access to the **Moment.js** library, **RSVP** promises, **fetch**, and **jQuery**.  When the button is pressed, there should be a popup that indicates the system is processing the request, the page should then include the JSON values of the rows in the output **&lt;div&gt;**, and then the popup should disappear.  When the button is pressed multiple times, it should only show the most recent results from the query.

You mission, should you choose to accept it, is to fix an issue that users complained about where the page seems to "hang" after pressing the button.  Please indicate any problems (including the root cause of the original bug) that appear in this code.  Fix all issues and provide us with the fixed code.  In addition, describe all of the problems you found.  Do not worry about schema names or shapes; you can assume the original coder referenced those correctly.  

## SQL
### Part 3: Queries
There are two `.csv` files in the `resources` directory.  Each one contains the contents of a PostgreSQL table that has the same name as the file without the `.csv`.  The first row gives the column names.  You can assume the column has whatever type makes the most sense.  The following problems will ask you to write queries against those in a database.  

Your query should run in LabKey, so you can use any generic SQL commands, along with any functions on [this page](https://www.labkey.org/Documentation/wiki-page.view?name=labkeysql).  You should make sure your query works in PostgreSQL.

Please comment your SQL.

### 3.1 Find overlaps
In the **assignments** table, there are sometimes animals assigned to multiple projects simultaneously.  Write a query that outputs a participant, the projects that were assigned at the same time, and the start and end date of that overlap.

### 3.2 Count Up Overlaps
Building on the previous query, provide a query that outputs two columns: the participant and the number of days that that participant was assigned to multiple projects.

### 3.3 Observations
We record our observations in an **obs** table.  Each observation is coded into a comma separated list of individual observation codes.  You need to write a query that counts the number of occurrences of each observation code for each animal.  You can assume that the only obs codes are the ones found in the `obs.csv` file.

In addition, describe any problems with that table, potential future problems in your query, and how they could be fixed.
