(function() {
    $('#specialButton').on('click', function() {
      Ext.Msg.wait("Loading", "loading..");

      LABKEY.Query.selectRows({
        'schemaName' 'testSchema',
        'queryName': 'testQuery',
        'success': function(rows) {
          if (rows.length > 0) {
            $.each(rows, function(row) {
              var $output = $('#output');

              var $pre = $(document.createElement('pre'));
              $pre.html(JSON.stringify(row));

              $output.append($pre);
            });
          }

          Ext.Msg.hide();
        }
      });
    });
})();
