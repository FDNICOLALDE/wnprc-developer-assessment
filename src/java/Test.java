import java.io.*;
import java.util.*;
import java.util.regex.*;

class Test {

    public static void main(String[] args) throws Exception {

        String filename = args[0]; String threshold = args[1];

        // Validate arguments
        if (Pattern.compile("^\\D+$").matcher(threshold).find()) {
            System.out.println("The threshold must be a number.");
            System.exit(1);
        }

        // Read file and tally word frequencies
        // Original case of word does not matter: "the" = "The" = "THE"
        File file = new File(filename);
        byte[] data = new byte[(int) file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(data);
        Vector<Word> words = new Vector<Word>();
        for (String line : new String(data).split("\\n")) {
            int found = 0;
            for (Word word : words) {
                if (word.w.equals(line.toLowerCase())) {
                    found = 1;
                    word.i++;
                }
            }

            // initialize a new word with a frequency of 1
            if (found != 1) {
                words.add(new Word(line, 1));
            }
        }

        // Print words and their frequencies, sorted alphabetically by word.  Only
        // print a word if its frequency is greater than or equal to the threshold.
        Collections.sort(words);
        for (Word word : words) {
            if (word.w.compareTo(threshold) < 0) continue;
            System.out.format("%4d %s\n", word.i, word.w);
        }
    }

    static class Word implements Comparable<Word> {
        String w; int i;
        Word(String w, int i) {this.w = w; this.i = i;}
        public int compareTo(Word a) {return w.compareTo(a.w);}
    }
}
